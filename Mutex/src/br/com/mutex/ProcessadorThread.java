package br.com.mutex;

/*
Integrantes: 
    - Gabriel Ribeiro de Araújo  
    - Renê Boaventura Junior
    - Wagner Dantas da Silva Mascarenhas

    Referência: https://brizeno.wordpress.com/2011/09/25/praticando-concorrencia-em-java-semaforos/
*/
import java.util.concurrent.Semaphore;

//Implementação da Thread que utilizará o semáforo
public class ProcessadorThread extends Thread {

    private int idThread; //Identificador da Thread
    private Semaphore semaforo;//Referência a um semáforo que irá controlar o acesso as variáveis

    //Construtor da classe que recebe como parâmetros o id da Thread e uma referência para a classe Semaphore
    public ProcessadorThread(int id, Semaphore semaphore) {
        this.idThread = id;
        this.semaforo = semaphore;
    }
    
    //Metódo usado para simular um tempo de processamento da Thread
    private void processamento() {
        try {
            System.out.println("Thread #" + idThread + " processando");
            //Põe a Thread pra dormir por um tempo com o método sleep da classe Thread
            Thread.sleep((long) (Math.random() * 10000));//Sorteio de um número que será o parâmetro do método sleep.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    //Este método simula a entrada de uma Thread em uma região não crítica
    private void entrarRegiaoNaoCritica() {
        //Exibe na tela o id da Thread e informa que a mesma está entrando em uma região não crítica
        System.out.println("Thread #" + idThread + " em região não crítica");
        processamento();//Realiza um processamento qualquer
    }
    
    //Este método simula a entrada de uma Thread em uma região crítica
    private void entrarRegiaoCritica() {
        //Exibe na tela o id da Thread e informa que a mesma está entrando em uma região crítica
        System.out.println("Thread #" + idThread + " entrando em região crítica");
        processamento();//Realiza um processamento qualquer
        //Exibe na tela o id da Thread e informa que a mesma está saindo de uma região crítica
        System.out.println("Thread #" + idThread + " saindo da região crítica");
    }
    
    //Sobrescrita do metodo run da classe Thread que executa sempre que a Thread iniciar
    public void run() {
        entrarRegiaoNaoCritica();//A Thread entra em uma região não crítica
        try {
            //Requisição de acesso ao semáforo
            semaforo.acquire();
            //Após adquirida a permissão pela variável semaforo, a Thread entra na região crítica
            entrarRegiaoCritica();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            /*Após o processamento, a Thread sai da região crítica e libera o recurso do semáforo com o método release,
            indicando que o recurso está liberado
            */
            semaforo.release();
        }
    }

    public static void main(String[] args) {
        //Definição da quantidade de processos que podem estar em uma região crítica simultaneamente 
        int numPermissoes = 2; 
        //Definição da quantidade de processos
        int numProcessos = 6;
        /*Criação de uma instância do tipo Semaphore. A instância do objeto
        recebe como parâmetro a quantidade de processos que podem estar em uma região crítica simultaneamente
        */
        Semaphore semaforo = new Semaphore(numPermissoes);
        //Criação de um Array com tamanho para 6 processos
        ProcessadorThread[] processos = new ProcessadorThread[numProcessos];
        
        //Laço para criar os 6 processos e iniciá-los
        for (int i = 0; i < numProcessos; i++) {
            //Criação de uma Thread recebendo como parâmetro o id e uma instância de semaforo para realizar o processamento
            processos[i] = new ProcessadorThread(i, semaforo);
            //O método start da classe Thread inicia a execução da Thread chamando o método sobrescrito run
            processos[i].start();
        }
    }
}
